# Argument(s)
# 1. farm_name
# 2. instance_name

FARM=$1
FLAV=$2
INST=${FARM}-${FLAV}
MACHINE="abacus"

GK=/home/parenaudgoud/animal-spot/g5k
TRAIN=/home/parenaudgoud/animal-spot/TRAINING


TEMPLATE=${GK}/template-instance.ini
FARM_INI=${GK}/${FARM}.ini
FLAV_INI=${GK}/${FLAV}.ini
MACHINE_INI=${GK}/${MACHINE}.ini
FULL_INI=${GK}/config-${INST}.ini
AS_INI=${FULL_INI}.as

module load conda
conda activate as

max_train_epochs=100

cat ${TEMPLATE} > ${FULL_INI}
echo "" >> ${FULL_INI}
echo "[machine]" >> ${FULL_INI}
cat ${MACHINE_INI} >> ${FULL_INI}
echo "" >> ${FULL_INI}
echo "[farm]" >> ${FULL_INI}
echo "name=${FARM}" >> ${FULL_INI}
echo "max_train_epochs=${max_train_epochs}" >> ${FULL_INI}
if [ -f "${FARM_INI}" ]; then
	cat ${FARM_INI} >> ${FULL_INI}
fi
echo "[flavor]" >> ${FULL_INI}
echo "name=${FLAV}" >> ${FULL_INI}
if [ -f "${FLAV_INI}" ]; then
	cat ${FLAV_INI} >> ${FULL_INI}
fi


echo "generate instance"
python ${GK}/generate_as_cfg_instance.py ${FULL_INI}
echo "generation done"
python ${TRAIN}/start_training.py ${FULL_INI}.as

export MKL_SERVICE_FORCE_INTEL=1
python ${GK}/predict.py ${FARM} ${FLAV}