FARM=$1
FLAV=$2

module load conda
conda activate as

export MKL_SERVICE_FORCE_INTEL=1

GK=/home/parenaudgoud/animal-spot/g5k
python ${GK}/predict.py ${FARM} ${FLAV}