import configparser
import sys, os

if __name__ == '__main__':
    if len(sys.argv) != 2:
        raise Exception('Invalid Number of Cmd Parameter. Only one argument -> path of config file')

    ini_path = sys.argv[1]
    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    config.read(ini_path)

    for _,path in config.items('path'):
        if not os.path.exists(path):
            os.mkdir(path)

    with open(f"{ini_path}.as", "w") as f:
        for k,path in config.items('aspath'):
            if not os.path.exists(path):
                os.mkdir(path)
            f.write(f"{k}={path}\n")

        for k,v in config.items('asparam'):
            if k in config['farm']:
                v = config['farm'].get(k)
            f.write(f"{k}={v}\n")

    data_src = os.path.join(config['machine'].get('init_data'), 'data')
    data_dst = config['aspath'].get('data_dir')
    os.system(f"rsync -r {data_src}/ {data_dst}")

    data_src = os.path.join(config['machine'].get('init_data'), 'background', f"*{config['farm'].get('name')}*")
    data_dst = config['aspath'].get('noise_dir')
    os.system(f"rsync -r {data_src} {data_dst}")
