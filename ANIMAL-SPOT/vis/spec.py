import os
import torch
import data.transforms as T
from data.audiodataset import AudioDataset, DefaultSpecDatasetOps
from utils.FileIO import AsyncFileReader
from typing import Any, Dict, Iterable, List
from collections import defaultdict
from utils.logging import Logger

import pandas as pd
import soundfile as sf
import numpy as np
import resampy
import math

""" Load audio file  """
def load_audio_file_clip(file_name, start, end, sr=None, mono=True):
    y, sr_orig = sf.read(file_name,
                         start=math.floor(sr * start), stop=math.floor(sr * end),
                         always_2d=True, dtype="float32")
    if sr_orig != sr:
        y, sr_orig = sf.read(file_name,
                         start=math.floor(sr_orig * start), stop=math.floor(sr_orig * end),
                         always_2d=True, dtype="float32")
    if mono and y.ndim == 2 and y.shape[1] > 1:
        y = np.mean(y, axis=1, keepdims=True)
    if sr is not None and sr != sr_orig:
        y = resampy.resample(y, sr_orig, sr, axis=0, filter="kaiser_best")
    return torch.from_numpy(y).float().t()

class ClippedAudioDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        origin: str,
        working_dir=None,
        sr=44100,
        n_fft=1024,
        hop_length=172,
        freq_compression="linear",
        n_freq_bins=256,
        f_min=150,
        f_max=8000,
        min_snr=12,
        max_snr=-3,
        seq_len=128,
        augmentation=False,
        noise_files=[],
        min_max_normalize=False,
        *args,
        **kwargs
    ):

        self.n_fft = n_fft
        self.hop_length = hop_length
        self.f_min = f_min
        self.f_max = f_max
        self.n_freq_bins = n_freq_bins

        self.wave = os.path.join(working_dir, f"{origin}.wav")

        self.labels = pd.read_csv(os.path.join(working_dir, f"{origin}.txt"), sep="\t",
                                  names=["start", "end", "label"])

        self._logger = Logger("ANALYSIS")

        valid_freq_compressions = ["linear", "mel", "mfcc"]

        if freq_compression not in valid_freq_compressions:
            raise ValueError(
                "{} is not a valid freq_compression. Must be one of {}",
                format(freq_compression, valid_freq_compressions),
            )
        self.freq_compression = freq_compression


        self._logger.debug(
            "Number of clips : {}".format(len(self.labels))
        )

        self.augmentation = augmentation

        spec_transforms = [
            lambda l: load_audio_file_clip(self.wave, l['start'], l['end'], sr=sr),
            T.PreEmphasize(DefaultSpecDatasetOps["preemphases"]),
            T.Spectrogram(n_fft, hop_length, center=False),
        ]

        spec_noise_transforms = [
            lambda fn: T.load_audio_file(fn, sr=sr),
            T.PreEmphasize(DefaultSpecDatasetOps["preemphases"]),
            T.Spectrogram(n_fft, hop_length, center=False),
        ]

        self.file_reader = AsyncFileReader()

        self.t_spectrogram = T.Compose(spec_transforms)

        if augmentation:
            self._logger.debug("Init augmentation transforms for time and pitch shift")
            self.t_amplitude = T.RandomAmplitude(3, -6)
            self.t_timestretch = T.RandomTimeStretch()
            self.t_pitchshift = T.RandomPitchSift()
        else:
            self._logger.debug("Running without augmentation")

        if self.freq_compression == "linear":
            self.t_compr_f = T.Interpolate(
                n_freq_bins, sr, f_min, f_max
            )
        elif self.freq_compression == "mel":
            self.t_compr_f = T.F2M(sr=sr, n_mels=n_freq_bins, f_min=f_min, f_max=f_max)
        elif self.freq_compression == "mfcc":
            self.t_compr_f = T.Compose(
                T.F2M(sr=sr, n_mels=n_freq_bins, f_min=f_min, f_max=f_max)
            )
            self.t_compr_mfcc = T.M2MFCC(n_mfcc=32)
        else:
            raise "Undefined frequency compression"

        if augmentation:
            if noise_files:
                self._logger.debug("Init augmentation transform for random noise addition")
                self.t_addnoise = T.RandomAddNoise(
                    noise_files,
                    T.Compose(spec_noise_transforms),
                    T.Compose(self.t_timestretch, self.t_pitchshift, self.t_compr_f),
                    min_length=seq_len,
                    min_snr=min_snr,
                    max_snr=max_snr,
                    return_original=True
                )
                print("added")
            else:
                self.t_addnoise = None
                self._logger.debug("No noise augmentation")

        self.t_compr_a = T.Amp2Db(min_level_db=DefaultSpecDatasetOps["min_level_db"])

        if min_max_normalize:
            self.t_norm = T.MinMaxNormalize()
            self._logger.debug("Init min-max-normalization activated")
        else:
            self.t_norm = T.Normalize(
                min_level_db=DefaultSpecDatasetOps["min_level_db"],
                ref_level_db=DefaultSpecDatasetOps["ref_level_db"],
            )
            self._logger.debug("Init 0/1-dB-normalization activated")

        self.t_subseq = T.PaddedSubsequenceSampler(seq_len, dim=1, random=augmentation)

    def __len__(self):
        return len(self.labels)

    """
    Computes per filename the entire data preprocessing pipeline containing all transformations and returns the
    preprocessed sample as well as the ground truth label 
    """
    def __getitem__(self, idx):
        label = self.labels.iloc[idx]

        sample = self.t_spectrogram(label)

        if self.augmentation:
            sample = self.t_amplitude(sample)
            sample = self.t_pitchshift(sample)
            sample = self.t_timestretch(sample)
        sample = self.t_compr_f(sample)

        if (
            self.augmentation and self.t_addnoise is not None
        ):
            # here
            sample, ground_truth = self.t_addnoise(sample)
            if self.freq_compression != "mfcc":
                ground_truth = self.t_compr_a(ground_truth)
            else:
                ground_truth = self.t_compr_mfcc(ground_truth)
            ground_truth = self.t_norm(ground_truth)
        else:
            ground_truth = None
        if self.freq_compression != "mfcc":
            sample = self.t_compr_a(sample)
        else:
            sample = self.t_compr_mfcc(sample)
        sample = self.t_norm(sample)
        if ground_truth is not None:
            stacked = torch.cat((sample, ground_truth), dim=0)
            stacked = self.t_subseq(stacked)
            sample = stacked[0].unsqueeze(0)
            ground_truth = stacked[1].unsqueeze(0)
        else:
            sample = self.t_subseq(sample)
        
        return sample, label['label']

