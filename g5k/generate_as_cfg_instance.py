import configparser
import sys, os
import pandas as pd

if __name__ == '__main__':
    if len(sys.argv) != 2:
        raise Exception('Invalid Number of Cmd Parameter. Only one argument -> path of config file')

    ini_path = sys.argv[1]
    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    config.read(ini_path)

    df = pd.read_csv(config['machine'].get('noise_file'))
    idf = df[(df['flavor'] == config['flavor'].get('name')) & (df['farm'] == config['farm'].get('name'))]
    if len(idf) == 1:
        cs = idf.iloc[0]
        config.set('instance', 'min_snr', str(cs.min_snr))
        config.set('instance', 'max_snr', str(cs.max_snr))

    for _,path in config.items('path'):
        if not os.path.exists(path):
            os.mkdir(path)

    with open(f"{ini_path}.as", "w") as f:
        for k,path in config.items('aspath'):
            if not os.path.exists(path):
                os.mkdir(path)
            f.write(f"{k}={path}\n")

        for k,v in config.items('asparam'):
            if k in config['farm']:
                v = config['farm'].get(k)
            if k in config['flavor']:
                v = config['flavor'].get(k)
            if k in config['instance']:
                v = config['instance'].get(k)
            if k == "sequence_len":
                duration = v
            f.write(f"{k}={v}\n")
    
    farm_name = config['farm'].get('name')

    data_src = os.path.join(config['machine'].get('init_data'), 'background-cut', f"*{farm_name}*")
    data_dst = config['aspath'].get('noise_dir')
    os.system(f"rsync -r {data_src} {data_dst}")

    data_src = os.path.join(config['machine'].get('init_data'), 'input', f"{duration}ms", "soundwel")
    data_dst = config['aspath'].get('data_dir')
    os.system(f"rsync -r {data_src} {data_dst}")

    data_src = os.path.join(config['machine'].get('init_data'), 'input', f"{duration}ms", farm_name, "farm_noise")
    data_dst = config['aspath'].get('data_dir')
    os.system(f"rsync -r {data_src} {data_dst}")

