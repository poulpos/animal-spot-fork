while read farm; do
  echo $farm
  nbox=${farm:3:1}
  short=${farm:9}
  oarsub -n "B${nbox}-F${short}-HF" -l gpu=1,walltime=2 -q production "bash -l /home/parenaudgoud/animal-spot/g5k/launch_job_instance.sh $farm HF"
  oarsub -n "B${nbox}-F${short}-LF" -l gpu=1,walltime=2 -q production "bash -l /home/parenaudgoud/animal-spot/g5k/launch_job_instance.sh $farm LF"
done < current_all
