import configparser
import sys, os

def generate_predict(prediction_template_path,
                     prediction_path,
                     model_path, model_name,
                     input_path, input_name):

    tmp_path = "/tmp/farm_prediction.ini"
    with open(tmp_path, 'w') as tmp_file:
        tmp_file.write("[model]\n")
        tmp_file.write(f"name={model_name}\n")
        tmp_file.write(f"path={model_path}\n")
        tmp_file.write("[prediction]\n")
        tmp_file.write(f"path={prediction_path}\n")
        tmp_file.write("[input]\n")
        tmp_file.write(f"path={input_path}\n")
        tmp_file.write(f"name={input_name}\n")
        with open(prediction_template_path, 'r') as pred_file:
            tmp_file.write(pred_file.read())
    


    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    config.read(tmp_path)

    with open(config['config']['file'], "w") as f:
        for k,path in config.items('aspath'):
            if not os.path.exists(path):
                os.mkdir(path)
            f.write(f"{k}={path}\n")

        for k,v in config.items('asparam'):
            if k in config['model']:
                v = config['model'].get(k)
            f.write(f"{k}={v}\n")


    return config