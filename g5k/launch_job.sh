# Argument(s)
# 1. farm_name

FARM=$1
MACHINE="abacus"

GK=/home/parenaudgoud/animal-spot/g5k
TRAIN=/home/parenaudgoud/animal-spot/TRAINING


TEMPLATE=${GK}/template.ini
FARM_INI=${GK}/${FARM}.ini
MACHINE_INI=${GK}/${MACHINE}.ini
FULL_INI=${GK}/config-${FARM}.ini
AS_INI=${FULL_INI}.as

module load conda
conda activate as

max_train_epochs=0

for i in 1 2 3 4 5; do
	python ${GK}/reset_checkpoint.py ${FARM}
	phase=`sed -n ${i}p ${GK}/noise_level`
	min_snr=`echo ${phase} | awk '{ print $2 }'`
	max_snr=`echo ${phase}  | awk '{ print $3 }'`
	epochs=`echo ${phase} | awk '{ print $1 }'`
	max_train_epochs=`echo "${max_train_epochs} + ${epochs}" | bc -l`
	cat ${TEMPLATE} > ${FULL_INI}
	echo "" >> ${FULL_INI}
	echo "[machine]" >> ${FULL_INI}
	cat ${MACHINE_INI} >> ${FULL_INI}
	echo "" >> ${FULL_INI}
	echo "[farm]" >> ${FULL_INI}
	echo "name=${FARM}" >> ${FULL_INI}
	echo "min_snr=${min_snr}" >> ${FULL_INI}
	echo "max_snr=${max_snr}" >> ${FULL_INI}
	echo "max_train_epochs=${max_train_epochs}" >> ${FULL_INI}
	if [ -f "${FARM_INI}" ]; then
		cat ${FARM_INI} >> ${FULL_INI}
	fi

	python ${GK}/generate_as_cfg.py ${FULL_INI}
	python ${TRAIN}/start_training.py ${FULL_INI}.as
done
