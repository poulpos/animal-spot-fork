
import sys, os, time

if __name__ == '__main__':

    farm = sys.argv[1]
    if len(sys.argv) > 2:
        farm = f"{farm}-{sys.argv[2]}"

    as_src_path = '/home/parenaudgoud/animal-spot/ANIMAL-SPOT'
    sys.path.insert(1, as_src_path)

    from utils.checkpoints import CheckpointHandler
    from utils.logging import Logger

    checkpoint_dir = os.path.join("/home/parenaudgoud/checkpoint", farm)
    log = Logger("TRAIN", True, os.path.join("/tmp/parenaudgoud-runtime-dir", farm))
    cp = CheckpointHandler(checkpoint_dir, prefix="binary_classifier", logger=log)
    checkpoint = cp.read_latest()

    if checkpoint is not None:
        # checkpoint["trainState"]["scheduler"]['_last_lr'] = []
        checkpoint["trainState"]["scheduler"]['best'] = 0
        checkpoint["trainState"]["scheduler"]['num_bad_epochs'] = 0
        checkpoint["trainState"]["scheduler"]['cooldown_counter'] = 0
        # checkpoint['trainState']['optState']['param_groups'][0]['lr'] = 0.001

        cp.write(checkpoint)
        time.sleep(10)