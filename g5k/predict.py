import sys, os, glob
import pandas as pd
import numpy as np
from typing import Iterable
import math
import configparser
from functools import reduce
import pickle as pck


if __name__ == '__main__':
    home = '/home/parenaudgoud'
    as_src_path = os.path.join(home, 'animal-spot')
    as_pred_path = os.path.join(as_src_path, 'PREDICTION')
    sys.path.insert(1, as_src_path)
    sys.path.insert(1, as_pred_path)
    result_path = home
    prediction_path = os.path.join(result_path, "prediction")
    prediction_template_path = os.path.join(as_src_path, "g5k", "prediction-template-g5k.ini")
    flavor_path = os.path.join(as_src_path, "g5k")
    inputs_path = os.path.join(home, "validation")
    model_path = result_path

    farm = sys.argv[1]
    flavor = sys.argv[2]


    def generate_predict(prediction_template_path,
                        prediction_path,
                        model_path, model_name,
                        flavor_path, flavor_name,
                        input_path, input_name,
                        tmp_path):
        
        for sub in ["config", "output", "log", "result"]:
            if not os.path.isdir(os.path.join(prediction_path, sub)):
                os.mkdir(os.path.join(prediction_path, sub))

        with open(tmp_path, 'w') as tmp_file:
            tmp_file.write("[model]\n")
            tmp_file.write(f"name={model_name}\n")
            tmp_file.write(f"path={model_path}\n")
            tmp_file.write("[flavor]\n")
            tmp_file.write(f"name={flavor_name}\n")
            with open(os.path.join(flavor_path, f"{flavor_name}.ini"), 'r') as flav_file:
                tmp_file.write(flav_file.read())
            tmp_file.write("\n[prediction]\n")
            tmp_file.write(f"path={prediction_path}\n")
            tmp_file.write("[input]\n")
            tmp_file.write(f"path={input_path}\n")
            tmp_file.write(f"name={input_name}\n")
            with open(prediction_template_path, 'r') as pred_file:
                tmp_file.write(pred_file.read())

        config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
        config.read(tmp_path)

        with open(config['config']['file'], "w") as f:
            for k,path in config.items('aspath'):
                if not os.path.exists(path):
                    os.makedirs(path, exist_ok=True)
                f.write(f"{k}={path}\n")

            for k,v in config.items('asparam'):
                if k in config['model']:
                    v = config['model'].get(k)
                if k in config['flavor']:
                    v = config['flavor'].get(k)
                    if k == "sequence_len":
                        v = int(v) * 0.001
                f.write(f"{k}={v}\n")


        return config


    def read_output(file, start_line=1):
        with open(file) as f:
            lines = [line.rstrip() for i, line in enumerate(f) if i >= start_line - 1]
        df = pd.DataFrame(columns=["start", "end", "proba"])
        for line in lines:
            current = {}
            infos = line.split("|")
            if len(infos) >= 2 and infos[1] == "D":
                continue
            if len(infos) < 3:
                print(f"Can't understand the following line\n  {line}")
            infos = [e.strip() for e in infos[2].split(",")]
            for varval in infos:
                [var, val] = varval.split("=")
                if var == "time":
                    [start, end] = val.split("-")
                    current['start'] = float(start)
                    current['end'] = float(end)
                elif var == "prob":
                    current['proba'] = float(val)
            row = pd.DataFrame(data={k:[v] for k,v in current.items() if k != "lr"})
            df = pd.concat([df, row], ignore_index=True)
        return df

    def aggregate(df, fun):
        timing = np.sort(pd.unique(pd.concat((df['start'],df['end']))))
        timing = pd.DataFrame(zip(timing[:-1], timing[1:]), columns=["start", "end"])
        full = pd.DataFrame(columns=["start", "end", "proba"], dtype=np.float64, data=[[0,0,0]])
        df["duration"] = df["end"] - df["start"]
        timing["duration"] = timing["end"] - timing["start"]
        df.sort_values(by='start', inplace=True)
        overlap_threshold = .8
        for i, slot in timing.iterrows():
            cut = df[(df['end'] >= slot.start) &  (df['start'] <= slot.end)]
            for j, wdw in cut.iterrows():
                dur_inter = min(slot.end, wdw.end) - max(slot.start, wdw.start)
                if dur_inter >= min(slot.duration, wdw.duration) * overlap_threshold:
                    full = pd.concat([full, pd.DataFrame(data={'start': [slot.start], 'end': [slot.end], 'proba': [wdw.proba]})], ignore_index=True)
        
        full['mid'] = full.apply(lambda r: (r.end + r.start)/2, axis=1)
        full['inter'] = full.apply(lambda r: pd.Interval(r.start, r.end), axis=1)
        full['agg_inter'] = fun
        meanf = full.groupby(by=["mid"]).agg(fun)
        meanf.reset_index(inplace=True)
        return meanf

    def pred_threshold(df: pd.DataFrame, th: Iterable[float], inter_eps=.001):
        final = None
        for f in pd.unique(df.agg_inter):
            for t in th:
                fdf = df[(df['proba'] >= 0.01*t) & (df['agg_inter'] == f)].drop(columns=['mid', 'start', 'end'])
                if len(fdf) == 0:
                    continue
                agg_pass = []
                current_row = fdf.iloc[0].copy()
                for i in range(1, len(fdf)):
                    if abs(fdf.iloc[i].inter.left - current_row.inter.right) < inter_eps:
                        current_row.inter = pd.Interval(current_row.inter.left, fdf.iloc[i].inter.right)
                    else:
                        agg_pass.append(current_row)
                        current_row = fdf.iloc[i].copy()
                agg_pass.append(current_row)
                current_df = pd.concat([l for l in agg_pass], axis=1).T
                current_df['threshold'] = t
                if final is None:
                    final = current_df
                else:
                    final = pd.concat([final, current_df], ignore_index=True, copy=False)
        if final is not None:
            final.drop(columns=['proba'], inplace=True)
        return final


    from start_prediction import setup_prediction


    all_f = None
    tmp_dir = os.path.join("/tmp/parenaudgoud-runtime-dir/config")
    if not os.path.isdir(tmp_dir):
        os.mkdir(tmp_dir)
    prediction_cfg = generate_predict(prediction_template_path,
                        prediction_path,
                        model_path, farm,
                        flavor_path, flavor,
                        inputs_path, farm,
                        os.path.join(tmp_dir, f"config-predict-{farm}-{flavor}"))
    predicter = setup_prediction(prediction_cfg['config']['file'])
    predicter.init_logger()
    predicter.read_config()
    predicter.start_prediction()
    for log in glob.glob("*.log", root_dir=prediction_cfg['aspath']['output_dir']):
        df = read_output(os.path.join(prediction_cfg['aspath']['output_dir'], log), start_line=12)
        aggf = aggregate(df, 'max')
        ff = pred_threshold(aggf, np.arange(50, 100, 5), inter_eps=.001)
        if ff is None:
            continue
        ff['file'] = log[:-19]
        ff['farm'] = prediction_cfg['model']['name']
        ff['flavor'] = prediction_cfg['flavor']['name']
        if all_f is None:
            all_f = ff
        else:
            all_f = pd.concat([all_f, ff], ignore_index=True, copy=False)

    fafla = all_f[["farm", "flavor"]].drop_duplicates()
    farm_hl = []
    for farm in pd.unique(all_f["farm"]):
        if len(fafla[(fafla["farm"] == farm)]) == 2:
            farm_hl.append(farm)
    pk = ['file', 'farm', 'threshold', 'agg_inter']
    all_f.sort_values(by=pk + ['inter'], inplace=True)


    pk_v = all_f[pk].drop_duplicates()
    agg_pass = []
    for i, r in pk_v.iterrows():
        alg_res = all_f[reduce(lambda a,b: (all_f[b] == r[b]) & a, pk, len(all_f) * [True])]
        if len(alg_res) == 0:
            continue
        current_row = alg_res.iloc[0].copy()
        for i in range(1, len(alg_res)):
            if alg_res.iloc[i].inter.left <= current_row.inter.right:
                current_row.inter = pd.Interval(current_row.inter.left, alg_res.iloc[i].inter.right)
            else:
                agg_pass.append(current_row)
                current_row = alg_res.iloc[i].copy()
        agg_pass.append(current_row)

    combined = pd.concat([l for l in agg_pass], axis=1).T
    combined["flavor"] = "HL"

    with open(os.path.join(prediction_path, "result", f"{prediction_cfg['instance'].get('name')}.pk"), "wb") as file:
        pck.dump(pd.concat([all_f, combined], ignore_index=True, copy=False), file)
