import sys
import os
training_src_path = '/home/parenaudgoud/animal-spot/TRAINING'
sys.path.insert(1, training_src_path)

from start_training import setup_training
trainer = setup_training(os.path.join(training_src_path, "config"))
trainer.init_logger()
trainer.read_config()
trainer.start_training()